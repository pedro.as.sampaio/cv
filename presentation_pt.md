# Apresentação Profissional

## Introdução
Olá, sou Pedro Alexandre Da Silva Sampaio, um desenvolvedor de software experiente e administrador de sistemas de TI, com uma sólida formação em tecnologias .NET e administração de sistemas. Atualmente, trabalho na Parfois, onde tenho liderado projetos críticos para a melhoria da eficiência operacional da empresa.

## Experiência Profissional
Na Parfois, liderei o desenvolvimento de um sistema de gestão de cartas de porte em .NET 6, que integra múltiplas transportadoras e implementa APIs RESTful seguindo os princípios SOLID. Este projeto não só melhorou a rastreabilidade e eficiência dos processos logísticos, como também destacou a importância de uma arquitetura de software bem planeada para suportar o crescimento e a escalabilidade.

Neste projeto, utilizei Entity Framework em conjunto com ADO.NET para a gestão de dados e construí uma connection pool para otimizar o desempenho da aplicação. A API foi desenvolvida com base nos princípios REST, incluindo protocolos (inpepotencia), hypermedia, e stateless, além de implementar o conceito de ETag para validação do estado do recurso.

A arquitetura foi cuidadosamente desenhada para separar a API da aplicação, do domínio e do repositório, com os serviços funcionando por injeção de dependência. Utilizei padrões de design como inversão de dependência, composition, e singletons. Para gerir o ciclo de vida das dependências, utilizei transients e scoped, além de assegurar a segurança com a utilização de JWT (JSON Web Tokens) para autenticação.

Implementei também a normalização de exceções utilizando códigos HTTP REST, criando um middleware próprio para lidar com exceções de forma global, garantindo uma resposta consistente e segura aos erros no sistema. Adicionalmente, implementei o Serilog para logging estruturado, com logs locais e exportação para Grafana Loki, permitindo um monitoramento e análise detalhados dos dados de log. Em todos os projetos, utilizo o Git para controlo de versões, garantindo uma gestão eficiente das mudanças no código e colaboração em equipa.

Além disso, implementei um sistema de mensagens RabbitMQ junto com as APIs dos trackings, melhorando a comunicação e integração entre sistemas. As aplicações estão atualmente alojadas em IIS, mas já estão preparadas para serem migradas para o Oracle OpenShift (Kubernetes). Participei também em formações (não certificadas) oferecidas pelo fornecedor sobre Oracle OpenShift.

### Projetos Anteriores e Competências
Anteriormente, na Plástirso, desenvolvi um sistema de gestão de itens e localizações para o armazém, além de um simulador de armazém que otimizou o uso do espaço e as operações. Estes projetos permitiram-me aplicar os meus conhecimentos em virtualização com VMware ESXi e em gestão de infraestrutura de TI.

Na Kerigma, atuei como consultor e especialista em suporte de TI, onde conduzi migrações de sistemas, reestruturei processos de backup e criei documentação técnica para padronizar operações e facilitar o treinamento.

## Curiosidade
Nos meus projetos, os padrões de design que mais utilizo incluem Adapter (RMWS + SIILOG), Bridge + Facade, Prototype (no .NET 6 não copia as classes internas), Singleton, e Factories.

## Educação e Formação Contínua
Tenho uma licenciatura em Informática e uma pós-graduação em Virtualização e Computação em Nuvem. Além disso, mantenho-me atualizado com cursos como "Advanced Process Flowcharts", "Test-Driven Development in .NET Core", e leituras relevantes como "Clean Code" e "Design Patterns".

## Conclusão e Objetivos
Estou sempre à procura de novas oportunidades para aplicar os meus conhecimentos e aprender novas tecnologias. O meu objetivo é continuar a contribuir para projetos desafiadores e inovadores, onde possa aplicar a minha experiência em desenvolvimento de software e administração de sistemas para criar soluções eficazes e escaláveis.

Se tiver a oportunidade de trabalhar convosco, estou certo de que vou aprender imenso com profissionais experientes e conhecedores.
