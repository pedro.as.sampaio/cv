# Pedro Alexandre da Silva Sampaio

**Contact Information:**

- **Address:** Rua Nossa Senhora Apresentação Nº 630 Viatodos, 4775-258 Viatodos Barcelos.
- **Phone:** +351 925 888 524
- **Email:** pedro.as.sampaio@gmail.com
- **LinkedIn:** [ Pedro Sampaio | LinkedIn](https://www.linkedin.com/in/pedro-sampaio-2b1965b1/))
- **GitLab:** [Pedro Sampaio · GitLab](https://gitlab.com/pedro.as.sampaio)

---

## Professional Summary

**Experienced Software Developer | Porto, Portugal**

Highly skilled software developer with a strong background in .NET 6+ and PL/SQL, backed by over 2 years of hands-on experience at Parfois, a renowned fashion accessories retailer. Proven expertise in developing and implementing robust software solutions for complex business needs. Notable projects include:

- **Waybill Management System**: Orchestrated the development of a comprehensive waybill management system integrating multiple carriers, utilizing RESTful model and SOLID principles. Implemented key features including authentication, structured logging, and connection pools, ensuring smooth operation and efficient tracking.

- **Logistics Labeling App**: Spearheaded the development of a web-based logistics labeling application, incorporating ZPL label templates and a dynamic relabeling system. Instrumental in building validation tasks, warehouse labels, and a quality module, streamlining labeling processes and enhancing operational efficiency.


Prior to Parfois, contributed significantly to optimizing operations and enhancing technological infrastructure at Plástirso, a plastic products manufacturer, as well as at Kerigma, a social innovation institute. Skilled in ASP.NET C#, VBScript, MS SQL, JavaScript, and HTML, with a proven track record in developing and deploying innovative solutions.

---

## Technical Skills

**Programming Languages:**

- .NET 6+
- ASP.NET C#
- PL/SQL
- VBScript
- JavaScript
- HTML

**Frameworks and Tools:**

- RESTful APIs
- SOLID Principles
- Serilog
- JWT Bearer Token Authentication
- ZPL Label Templates
- WebAPI
- WPF (Windows Presentation Foundation)

**Databases:**

- Microsoft SQL Server
- Oracle

**Other Competencies:**

- Database Management (Microsoft SQL Server, MySQL)
- Software Development Lifecycle (SDLC)
- Object-Oriented Programming (OOP)
- Agile Methodologies
- Version Control Systems (e.g., Git)
- Problem-Solving and Troubleshooting Skills

---

## Professional Experience

### **Software Developer**  
_Parfois, Porto, Portugal_  
_March 2022 - Present_

- Spearheaded the development of a comprehensive waybill management system integrating multiple carriers, including CttExpress, CorreosExpress, DPD, and DHL24.
- Designed and implemented RESTful APIs including WaybillAPI, TrackingAPI, and SSOAPI, adhering to SOLID principles.
- Implemented key features such as authentication using JWT bearer tokens, connection pooling, and structured logging with Serilog.
- Developed a logistics labeling application utilizing ZPL label templates and a dynamic relabeling system.
- Led the development of validation tasks, warehouse labels, and a quality module, improving labeling processes.
- Collaborated with cross-functional teams to ensure smooth deployment and operation of software solutions.

**Systems Technician and Information Technology**  
_Plastirso, Trofa, Portugal_  
_January 2018 - March 2022_

- Developed and implemented an Item and Location Management System for warehouse operations, integrating with ERP systems.
- Led the development of a Warehouse Simulator, optimizing warehouse space and operations.
- Evaluated and implemented TSPLUS remote environment and VMWare ESXi for datacenter infrastructure.
- Implemented NAKIVO software for real-time virtual machine backups, enhancing data security.
- Configured and managed WiFi services, including management software, VLANs, and access points.
- Integrated attendance control systems with the intranet, ensuring GDPR compliance.

**Computer Consultant**  
_Kerigma - Instituto de Inovação e Desenvolvimento Social de Barcelos, Barcelos, Portugal_  
_January 2017 - September 2017_

- Led projects focused on data network documentation and email service migration, improving communication and efficiency within the organization.

---

## Projects

**Waybill Management System**

- _Parfois, Porto, Portugal_
- Developed a comprehensive waybill management system integrating multiple carriers such as CttExpress, CorreosExpress, DPD, and DHL24.
- Designed and implemented RESTful APIs including WaybillAPI, TrackingAPI, and SSOAPI, following SOLID principles.
- Implemented key features such as authentication using JWT bearer tokens, connection pooling, and structured logging with Serilog.

**2. Logistics Labeling App**

- _Parfois, Porto, Portugal_
- Led the development of a logistics labeling application utilizing ZPL label templates and a dynamic relabeling system.
- Developed validation tasks, warehouse labels, and a quality module, improving labeling processes and efficiency.

**3. Item and Location Management System for Warehouse**

- _Plastirso, Trofa, Portugal_
- Developed and implemented a system to manage items and locations within the warehouse, integrating with ERP systems for seamless operations.
- Developed tablet applications and solutions for mounting on electric forklifts, enhancing efficiency in warehouse operations.

**4. Warehouse Simulator**

- _Plastirso, Trofa, Portugal_
- Created a warehouse simulation tool to optimize space utilization and operational efficiency.
- Simulated occupied locations based on current stock and application rules, incorporating parameters such as pallet type, maximum weight, and height.

**5. Data Network Documentation**

- _Kerigma - Instituto de Inovação e Desenvolvimento Social de Barcelos, Barcelos, Portugal_
- Led a project focused on documenting the data network infrastructure, improving organization and accessibility of network-related information.

**6. Email Service Migration**

- _Kerigma - Instituto de Inovação e Desenvolvimento Social de Barcelos, Barcelos, Portugal_
- Played a key role in migrating the organization's email services to an outsourced provider, ensuring smooth transition and minimal disruption.

---

## Education

**Postgraduate Degree in Virtualization and Cloud Computing**  
_ISTEC, Lisbon, Portugal_  
_2019 - 2020_

- Received a grade of 16.
- Covered topics including Introduction to Cloud Computing, Private, Hybrid, and Public Cloud Computing, and Virtualization.
- Specialized in Microsoft Hyper-V, VMware ESXi, Microsoft System Center Virtual Machine Manager (SCVMM), and Microsoft System Center Operations Manager (SCOM).

**Bachelor's Degree in Computer Science**  
_ISTEC, Lisbon, Portugal_  
_2019_

- Courses included Programming I, II, III & IV (C++, C#, ASP.NET), Mobile Applications Development, Computer Security, Network Administration I & II (Windows Server: Active Directory, File and Storage, Windows Server Update Services), Database (Microsoft SQL), Management Systems of Database - Microsoft® SQL Server®, Algorithms and Data Structures, Computer Architecture, Financial Calculus, Cryptography, Computer Law, Descriptive Statistics, Business Management, Introduction to Computer Science, Mathematics I & II, Networks and Communications I & II, Multimedia Systems I & II, Internet Technologies I, II, III & IV (JavaScript, Processing), Probability Theory and Simulation Models, and Computational Models and Simulations.